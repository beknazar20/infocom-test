
export default  {
    state:{
        organizations: [
            {
                id: 1,
                typeOfOwnership: 'dsgdfhgf',
                legalForm: 'bdfbfdb',
                name: 'something 22',
                nameOfHead: 'bfdbdsfb',
                fax: '34543',
                phone: '2346534634',
                webPageLink: 'gertbrg',
                licenseNumber: '2435435234',
                DateOfLicense: '324234',
                certificateNumber: '3245324t',
                DateOfCertificate: '534523',
                address: 'rdghnbtr',
                employeesList:[]
            }
        ]
    },

    mutations: {
        createNewOrganization(state, organization) {
            state.organizations.push(organization)
        },
        editOrganizationM(state, payload) {
            const organizations = JSON.parse(JSON.stringify(state.organizations))
            const currentEl = organizations.findIndex(c => c.id == payload.id);
            state.organizations.splice(currentEl, 1, payload);
        },
        createNewEmployee(state, employee) {
            const organization = JSON.parse(JSON.stringify(state.organizations))
            const el = organization.find(i => i.id == employee.organizationId)
            el.employeesList.push({...employee.employee});

        },
        deleteOrganization(state, deleteId) {
            const organizations = JSON.parse(JSON.stringify(state.organizations))
            let el = organizations.filter(c => c.id !== deleteId);
            state.organizations = el
        },
        getOneOrganization(state, id) {
            const organizations = JSON.parse(JSON.stringify(state.organizations))
            return organizations.find(item => item.id === id);
        },
    },
    actions: {
        storeOrganization({commit}, organization) {
            commit('createNewOrganization', organization);
        },
        editOrganization({commit}, newOrganizationInfo) {
            commit('editOrganizationM', newOrganizationInfo);
        },
        // eslint-disable-next-line no-unused-vars
        storeNewEmployee({_, state}, employee) {
            console.log('payload in action', employee)
            const organizations = JSON.parse(JSON.stringify(state.organizations))
            const el = organizations.find(i => i.id == employee.organizationId)
            el.employeesList.push({...employee.employee});
            console.log(el.employeesList) // here adding employee for organization working, but in UI component dont show actual emplyee array
        },
        deleteOrganization({commit}, organizatoin) {
            commit('deleteOrganization', organizatoin);
        },
        getOrganizationById({commit}, organizatoin) {
            commit('getOneOrganization', organizatoin);
        }
    },

    getters: {
        getAllOrganizatoins(state) {
            return state.organizations;
        },
        organizationById (state) {
            const organization = JSON.parse(JSON.stringify(state.organizations))
                return (id) => {
                    return organization.find(item => item.id == id);
                }
            }
    },
}