import Vuex from "vuex"
import organization from './modules/organization'
export default new Vuex.Store({
    modules: {
        organization
    }
})