import {createRouter, createWebHashHistory} from 'vue-router'
import MainPage from "@/pages/MainPage";
import OrganizationDetailsPage from "@/pages/OrganizationDetailsPage";
import OrganizationCreatePage from "@/pages/OrganizationCreatePage";
import OrganizationEditPage from "@/pages/OrganizationEditPage";
import EmployeeAddPage from "@/pages/EmployeeAddPage";
export default new createRouter({
    history:createWebHashHistory(),
    routes: [
        {
            path: '/',
            component: MainPage
        },
        {
            path: '/create',
            component: OrganizationCreatePage
        },
        {
            path: '/details/:id',
            component: OrganizationDetailsPage
        },
        {
            path: '/edit/:id',
            component: OrganizationEditPage
        },
        {
            path: '/details/add-employee/:id',
            component: EmployeeAddPage
        }
    ]

})